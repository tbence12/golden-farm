package org.goldenfarm.service;

import java.util.List;

import javax.annotation.PostConstruct;

import org.goldenfarm.model.Golden;
import org.goldenfarm.model.Owner;
import org.goldenfarm.repository.GoldenRepository;
import org.goldenfarm.repository.OwnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoldenFarmService {

	private GoldenRepository goldenRepo;
	private OwnerRepository ownerRepo;
	
	@Autowired
	public void setGoldenRepo(GoldenRepository goldenRepo) {
		this.goldenRepo = goldenRepo;
	}
	
	@Autowired
	public void setOwnerRepo(OwnerRepository ownerRepo) {
		this.ownerRepo = ownerRepo;
	}
	
	public List<Golden> getGoldens(){
		return goldenRepo.findAll();
	}
	
	@PostConstruct
	public void init() {
		Owner owner1 = new Owner("Mari",22);
		ownerRepo.save(owner1);
		Owner owner2 = new Owner("Piri",21);
		ownerRepo.save(owner2);
		
		Golden golden1 = new Golden("#1","Sunyi",2,"Ül","<img src=\"img/1.jpg\" height=\"200\" width=\"350\">",owner1);
		goldenRepo.save(golden1);
		Golden golden2 = new Golden("#2","Szundi",8,"Alszik","<img src=\"img/2.jpg\" height=\"200\" width=\"350\">",owner1);
		goldenRepo.save(golden2);
		Golden golden3 = new Golden("#3","Sanyi",6,"Eszik","<img src=\"img/3.jpg\" height=\"200\" width=\"350\">",owner2);
		goldenRepo.save(golden3);
		Golden golden4 = new Golden("#4","Rexi",3,"Szalad","<img src=\"img/4.jpg\" height=\"200\" width=\"350\">",owner2);
		goldenRepo.save(golden4);
		Golden golden5 = new Golden("#5","Bundi",5,"Fekszik","<img src=\"img/5.jpg\" height=\"200\" width=\"350\">",owner1);
		goldenRepo.save(golden5);
	}
	
	
}

package org.goldenfarm.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Owner {
	
	@GeneratedValue
	@Id
	private Long id;
	private String name;
	private int age;
	@OneToMany(mappedBy = "owner")
	private List<Golden> goldens;
	
	private Owner() {}

	public Owner(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public List<Golden> getGoldens() {
		return goldens;
	}

	public void setGoldens(List<Golden> goldens) {
		this.goldens = goldens;
	}
	
	
}

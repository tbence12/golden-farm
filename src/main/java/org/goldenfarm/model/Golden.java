package org.goldenfarm.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity(name = "Golden")
public class Golden {
	
	@GeneratedValue
	@Id
	private Long id;
	private String goldenId;
	private String name;
	private int age;
	private String ability;
	private String picture;
	@ManyToOne
	private Owner owner;
	
	private Golden() {}

	public Golden(String goldenId, String name, int age, String ability, String picture, Owner owner) {
		this.goldenId = goldenId;
		this.name = name;
		this.age = age;
		this.ability = ability;
		this.picture = picture;
		this.owner = owner;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGoldenId() {
		return goldenId;
	}

	public void setGoldenId(String goldenId) {
		this.goldenId = goldenId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getAbility() {
		return ability;
	}

	public void setAbility(String ability) {
		this.ability = ability;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}
	

}

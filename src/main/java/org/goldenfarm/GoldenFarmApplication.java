package org.goldenfarm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoldenFarmApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoldenFarmApplication.class, args);
	}
}

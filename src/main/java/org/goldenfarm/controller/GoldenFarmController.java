package org.goldenfarm.controller;

import org.goldenfarm.service.GoldenFarmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GoldenFarmController {
	
	private GoldenFarmService goldenFarmService;
	
	@Autowired
	public void setGoldenService(GoldenFarmService goldenFarmService) {
		this.goldenFarmService = goldenFarmService;
	}
	
	@RequestMapping("/")
	public String home() {
		return "index";
	}
	
	@RequestMapping("/index.html")
	public String index() {
		return "index";
	}
	
	@RequestMapping("/golden-list.html")
	public String goldies(Model model) {
		model.addAttribute("goldens",goldenFarmService.getGoldens());
		return "golden-list";
	}
	
	@RequestMapping("/golden-igeny.html")
	public String goldigeny() {
		return "golden-igeny";
	}
}

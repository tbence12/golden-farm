package org.goldenfarm.repository;

import java.util.List;

import org.goldenfarm.model.Golden;
import org.springframework.data.repository.CrudRepository;

public interface GoldenRepository extends CrudRepository<Golden, Long> {
	
	List<Golden> findAll();

}

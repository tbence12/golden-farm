package org.goldenfarm.repository;

import java.util.List;

import org.goldenfarm.model.Owner;
import org.springframework.data.repository.CrudRepository;

public interface OwnerRepository extends CrudRepository<Owner, Long> {
	
	List<Owner> findAll();

}
